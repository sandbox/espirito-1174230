<?php

/**
 * Address handler for Italy.
 */
class AddressFieldForm_cz implements AddressFieldForm {
  public function form($address, $field, $instance, $langcode, $items, $delta, $element) {
    $required = $delta == 0 && $instance['required'];

    $form['thoroughfare'] = array(
      '#title' => t('Address'),
      '#type' => 'textfield',
      '#size' => 30,
      '#required' => $required,
      '#default_value' => $address['thoroughfare'],
    );
    $form['locality'] = array(
      '#title' => t('City'),
      '#type' => 'textfield',
      '#size' => 30,
      '#required' => $required,
      '#default_value' => $address['locality'],
    );
    $form['postal_code'] = array(
      '#title' => t('Postal Code'),
      '#type' => 'textfield',
      '#size' => 10,
      '#required' => $required,
      '#default_value' => $address['postal_code'],
    );
    return $form;
  }
}
